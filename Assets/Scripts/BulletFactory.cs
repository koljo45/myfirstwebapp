﻿using System.Collections.Generic;
using UnityEngine;

public class BulletFactory : MonoBehaviour
{
    [SerializeField] private BulletController _bulletPrefab;

    [SerializeField] private float _bulletSpeed;

    public float BulletSpeed
    {
        get
        {
            return _bulletSpeed;
        }
    }

    private Stack<BulletController> _bulletStack = new Stack<BulletController>();

    public BulletController getBullet()
    {
        if (_bulletStack.Count > 0)
            return _bulletStack.Pop();

        BulletController bc = Instantiate(_bulletPrefab);
        bc.setParentFactory(this);
        return bc;
    }

    public void recycleBullet(BulletController bc)
    {
        if (_bulletStack.Contains(bc))
        {
            Debug.LogError("You tried to recycle a bullet before firing it!");
            return;
        }
        _bulletStack.Push(bc);
    }
}
