﻿using UnityEngine;

public class TrainingEnviromentController : MonoBehaviour
{
    [SerializeField] private TankAgent _AgentPrefab;
    [SerializeField] private Transform _ASideSpawnPoint;
    [SerializeField] private Transform _BSideSpawnPoint;
    [Range(6, 32)]
    [SerializeField] private int _ASideLayer = 6;
    [Range(6, 32)]
    [SerializeField] private int _BSideLayer = 7;

    void Start()
    {
        var agentA = Instantiate(_AgentPrefab, _ASideSpawnPoint.position, _ASideSpawnPoint.rotation);
        agentA.gameObject.layer = _ASideLayer;
        agentA.EnemyLayer = _BSideLayer;
        agentA.FriendlyLayer = _ASideLayer;

        var agentB = Instantiate(_AgentPrefab, _BSideSpawnPoint.position, _BSideSpawnPoint.rotation);
        agentB.gameObject.layer = _BSideLayer;
        agentB.EnemyLayer = _ASideLayer;
        agentB.FriendlyLayer = _BSideLayer;

    }
}
