﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform _target;
    private Rigidbody _rigidbody;
    public Transform Target
    {
        get
        {
            return _target;
        }
        set
        {
            _target = value;
            _rigidbody = value.gameObject.GetComponent<Rigidbody>();
        }
    }

    [SerializeField] private float _distance = 3.0f;
    [SerializeField] private float _height = 3.0f;

    [SerializeField] private bool _smoothPosition = true;
    [SerializeField] private float _positionDamping = 5.0f;
    [SerializeField] private bool _smoothRotation = true;
    [SerializeField] private float _rotationDamping = 10.0f;

    [SerializeField] private Vector3 _rotationOffset;
    [SerializeField] private bool _followBehind = true;
    [SerializeField] private bool _matchTargetRotation = false;
    [SerializeField] private bool _useRigidbody = false;

    [Tooltip("Minimum velocity that affects the camera position when a rigidbody is attached to the target.")]
    [SerializeField] private float _minTrackingVelocity = 0.5f;

    private Transform _transform;
    private Quaternion _rotOffset;

    private void OnValidate()
    {
        _rotOffset = Quaternion.Euler(_rotationOffset);
    }

    private void Awake()
    {
        _transform = transform;
        _rotOffset = Quaternion.Euler(_rotationOffset);
    }

    void FixedUpdate()
    {
        if (Target == null)
        {
            return;
        }
        Vector3 wantedPosition;

        if (_rigidbody && _useRigidbody)
        {
            Vector3 dist = _rigidbody.velocity.normalized * _distance * (_followBehind ? -1 : 1);
            dist.y = _height;
            wantedPosition = Target.position + dist;
            if (_rigidbody.velocity.magnitude < _minTrackingVelocity)
                wantedPosition = Target.TransformPoint(0, 0, _distance * (_followBehind ? -1 : 1)) + Vector3.up * _height;
        }
        else
            wantedPosition = Target.TransformPoint(0, 0, _distance * (_followBehind ? -1 : 1)) + Vector3.up * _height;

        if (_smoothPosition)
            _transform.position = Vector3.Lerp(_transform.position, wantedPosition, Time.deltaTime * _positionDamping);
        else
            _transform.position = wantedPosition;

        if (_smoothRotation)
        {
            Quaternion wantedRotation;
            if (_matchTargetRotation)
                wantedRotation = Target.rotation;
            else
                wantedRotation = Quaternion.LookRotation(Target.position - _transform.position, Target.up);

            wantedRotation *= _rotOffset;

            _transform.rotation = Quaternion.Slerp(_transform.rotation, wantedRotation, Time.deltaTime * _rotationDamping);
        }
        else
        {
            if (_matchTargetRotation)
                _transform.rotation = Target.rotation;
            else
                _transform.LookAt(Target, Target.up);
            _transform.rotation *= _rotOffset;
        }
    }
}
