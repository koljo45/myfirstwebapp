﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TankController : MonoBehaviour
{
    [SerializeField] private Transform _barrel;
    [SerializeField] private ParticleSystem _muzzleFlash;
    [SerializeField] private BulletFactory _bulletFactory;

    [SerializeField] private float _movementSpeed = 8;
    [SerializeField] private float _rotationSpeed = 0.5f;
    [SerializeField] private float _accelerationFactor = 1;
    [SerializeField] private float _steeringFactor = 1;
    [SerializeField] private float _reloadTime = 5;

    private Transform _transform;
    private Rigidbody _rigidbody;
    private bool _canShoot = true;
    void Awake()
    {
        _transform = transform;
        _rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float forward = Mathf.Clamp(Input.GetAxis("Vertical") * _accelerationFactor, -1, 1);
        float steering = Mathf.Clamp(Input.GetAxis("Horizontal") * _steeringFactor, -1, 1);
        float shoot = Input.GetAxis("Fire1");

        //_rigidbody.AddForce(_transform.forward* forward * _speed);
        _rigidbody.MovePosition(_transform.position + _transform.forward * forward * _movementSpeed * Time.deltaTime);

        _rigidbody.MoveRotation(_transform.rotation * Quaternion.Euler(0, steering * _rotationSpeed, 0));

        if (shoot > 0)
            Shoot();
    }

    private void Shoot()
    {
        if (!_canShoot)
            return;

        BulletController bullet = _bulletFactory.getBullet();
        bullet.Initialize(_barrel.position, Quaternion.LookRotation(_barrel.forward));
        bullet.Shoot();
        _muzzleFlash.Play();
        _canShoot = false;
        StartCoroutine(reload());
    }

    private IEnumerator reload()
    {
        yield return new WaitForSeconds(_reloadTime);
        _canShoot = true;
    }
}
