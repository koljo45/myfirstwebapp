﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class BulletController : MonoBehaviour
{
    private BulletFactory _parentFactory;

    private Transform _transform;
    private Rigidbody _rigidbody;
    private MeshRenderer _renderer;
    private Collider _collider;

    private ParticleSystem _explosionSystem;

    public bool tankHit = false;

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        _collider.isTrigger = true;
        _transform = transform;
        _rigidbody = GetComponent<Rigidbody>();
        _renderer = GetComponentInChildren<MeshRenderer>();
        _explosionSystem = GetComponentInChildren<ParticleSystem>();
    }

    public void Initialize(Vector3 position, Quaternion rotation)
    {
        _transform.SetPositionAndRotation(position, rotation);
        _rigidbody.isKinematic = true;
        _renderer.enabled = true;
        _collider.enabled = true;
    }

    public void Shoot()
    {
        _rigidbody.isKinematic = false;
        _rigidbody.velocity = _transform.forward * _parentFactory.BulletSpeed;
    }

    // Nakon ekslozije treba reciklirati metak
    private IEnumerator disposeTimer(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        _parentFactory.recycleBullet(this);
    }

    public void OnTriggerEnter(Collider other)
    {
        // Pri sudaru pokretanje ekslozije, onemogucavanje danjeg kretanja te onemoguciti prikaz metka jer je upravo eksplodirao
        _explosionSystem.Play();
        _rigidbody.isKinematic = true;
        _collider.enabled = false;
        _renderer.enabled = false;
        StartCoroutine(disposeTimer(_explosionSystem.main.duration));

        if(other.gameObject.layer >= 6)
        {
            tankHit = true;
        }
    }

    public void setParentFactory(BulletFactory factory)
    {
        _parentFactory = factory;
    }
}
