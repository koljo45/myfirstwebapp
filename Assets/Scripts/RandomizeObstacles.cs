﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class RandomizeObstacles : MonoBehaviour
{
    //looking from spawn A to spawn B
    public List<GameObject> lanes;
    public GameObject leftWall;
    public GameObject rightWall;


    void Start()
    {
        var minX = leftWall.transform.localPosition.x;
        var maxX = rightWall.transform.localPosition.x;

        foreach (var lane in lanes)
        {
            var obstacles = lane.GetComponentsInChildren<Transform>();

            foreach (var obstacle in obstacles)
            {
                if(obstacle.name.ToLower().Contains("lane"))
                {
                    continue;
                }
                var minObstacleX = getMinObstacleX(obstacle, minX);
                var maxObstacleX = getmMaxObstacleX(obstacle, maxX);

                var temp = UnityEngine.Random.Range(minObstacleX, maxObstacleX);

                obstacle.localPosition = new Vector3(temp, obstacle.localPosition.y, obstacle.localPosition.z);
            }
        }
    }

    private float getmMaxObstacleX(Transform obstacle, float maxX)
    {
        var obstacleXScale = obstacle.localScale.x;
        return maxX - (obstacleXScale / 2);
    }

    private float getMinObstacleX(Transform obstacle, float minX)
    {
        var obstacleXScale = obstacle.localScale.x;
        return minX + (obstacleXScale / 2);
    }
}
