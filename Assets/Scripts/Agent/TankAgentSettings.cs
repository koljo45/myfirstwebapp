﻿using UnityEngine;

public class TankAgentSettings : ScriptableObject
{
    [SerializeField] private float _movementSpeed;
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _raycastDistance;

    public float MovementSpeed
    {
        get
        {
            return _movementSpeed;
        }
    }

    public float RotationSpeed
    {
        get
        {
            return _rotationSpeed;
        }
    }

    public float RaycastDistance
    {
        get
        {
            return _raycastDistance;
        }
    }
}
