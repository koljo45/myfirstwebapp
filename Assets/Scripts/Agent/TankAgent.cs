using System.Collections;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TankAgent : Agent
{
    public enum TargetType { ENEMY = 0, OBSTACLE = 1, FRIENDLY = 2, NONE = 3 };

    [SerializeField] private Transform _barrel;
    [SerializeField] private BulletFactory _bulletFactory;

    // Inited by TrainingEnviromentController
    [HideInInspector] public LayerMask FriendlyLayer;
    [HideInInspector] public LayerMask EnemyLayer;

    private EnvironmentParameters _ResetParams;
    private Rigidbody _rigidbody;
    private Transform _transform;
    private Vector3 _startPost;
   

    // Moguce da nije potrebno cuvati ove informacije u svakoj instanci.
    // TankAgentSettings su dodani za djeljenje informacija izmedu instanci, treba skontati kako se _ResetParams mjenjaju.
    private float _movementSpeed;
    private float _rotationSpeed;
    private float _raycastDistance;

    private bool _canShoot = true;
    [SerializeField] private float _reloadTime = 5;

    public override void Initialize()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _transform = transform;
        _barrel = FindChildByRecursion(transform, "Fire_Point");
        _bulletFactory = GameObject.Find("BulletFactory1").GetComponent<BulletFactory>();
        _ResetParams = Academy.Instance.EnvironmentParameters;
        _startPost = transform.position;
        SetResetParameters();
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        RaycastHit hit;
        bool hitSucc = Physics.Raycast(_barrel.position, _barrel.forward, out hit, _raycastDistance);

        TargetType tt = TargetType.NONE;

        if (hitSucc)
        {
            tt = TargetType.OBSTACLE;
            int layer = hit.transform.gameObject.layer;

            if (layer == EnemyLayer)
                tt = TargetType.ENEMY;
            else if (layer == FriendlyLayer)
                tt = TargetType.FRIENDLY;
        }

        sensor.AddObservation((int)tt);
        sensor.AddObservation(hit.distance);
        //Possibly more parameters needed
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        var forward = Mathf.Clamp(vectorAction[0], -1f, 1f);
        var steering = Mathf.Clamp(vectorAction[1], -1f, 1f);
        float shoot = Mathf.Clamp(vectorAction[2], 0f, 1f);


        _rigidbody.MovePosition(_transform.position + _transform.forward * forward * _movementSpeed * Time.deltaTime);

        _rigidbody.MoveRotation(_transform.rotation * Quaternion.Euler(0, steering * _rotationSpeed, 0));

        if(shoot > 0.75)
        {
            if(Shoot())
            {
                SetReward(5.0f);
                EndEpisode();
            } else
            {
                if(!_canShoot)
                {
                    SetReward(-0.1f);
                }
                else
                {
                    SetReward(-0.5f);
                }

            }
        }
        else
        {
            SetReward(-0.05f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("DoNotRunIntoThis"))
        {
            Debug.Log("Sudario se sa zidom!");
            AddReward(-3f);
        }
    }

    public override void OnEpisodeBegin()
    {

        //Reset the parameters when the Agent is reset.
        SetResetParameters();
    }

    public override void Heuristic(float[] actionsOut)
    {
        actionsOut[0] = Input.GetAxis("Vertical");
        actionsOut[1] = Input.GetAxis("Horizontal");
        actionsOut[2] = Input.GetAxis("Fire1");
    }

    public void SetTank()
    {
        //Set the attributes of the tank by fetching the information from the academy
        _movementSpeed = _ResetParams.GetWithDefault("movementSpeed", 8);
        _rotationSpeed = _ResetParams.GetWithDefault("rotationSpeed", 0.5f);
        _raycastDistance = _ResetParams.GetWithDefault("raycastDistance", 10);

        _rigidbody.velocity = Vector3.zero;
        _rigidbody.angularVelocity = Vector3.zero;
        gameObject.transform.position = _startPost;
        Debug.Log(_startPost);
        gameObject.transform.rotation = _transform.rotation;
    }

    public void SetResetParameters()
    {
        SetTank();
    }

    private bool Shoot()
    {
        if (!_canShoot)
            return false;

        BulletController bullet = _bulletFactory.getBullet();
        bullet.Initialize(_barrel.position, Quaternion.LookRotation(_barrel.forward));
        bullet.Shoot();
        _canShoot = false;
        StartCoroutine(reload());

        return bullet.tankHit;
    }

    private IEnumerator reload()
    {
        yield return new WaitForSeconds(_reloadTime);
        _canShoot = true;
    }

    static Transform FindChildByRecursion(Transform aParent, string aName)
    {
        if (aParent == null) return null;
        var result = aParent.Find(aName);
        if (result != null)
            return result;
        foreach (Transform child in aParent)
        {
            result = FindChildByRecursion(child, aName);
            if (result != null)
                return result;
        }
        return null;
    }

}
